# [Calendar of Harptos](https://forgottenrealms.fandom.com/wiki/Calendar_of_Harptos)

1.   Hammer     "Deepwinter"
2.   Alturiak   "The Claw of Winter"
3.   Ches       "The Claw of Sunsets"
4.   Tarsakh    "The Claw of Storms"
5.   Mirtul     "The Melting"
6.   Kythorn    "The Time of Flowers"
7.   Flamerule  "Summertide"
8.   Eleasis    "Highsun"
9.   Eleint     "The Fading"
10.  Marpenoth  "The Leaffall"
11.  Uktar      "The Rotting"
12.  Nightal    "The Drawing Down

- **Midwinter** between **Hammer 30** and **Alturiak 01**
- **Spring Equinox** on **Ches 19**
- **Greengrass** between **Tarsakh 30** and **Mirtul 01**
- **Summer Solstice** on **Kythorn 20**
- **Midsummer** between **Flamerule 30** and **Eleasis 01**
- **Shieldmeet** the day after **Midsummer**, 1/4 years
- **Autumn Equinox** on **Eleint 21**
- **Highharvestide** between **Eleint 30** and **Marpenoth 01**
- **Feast of the Moon** between **Uktar 30** and **Nightal 01**
- **Winter Solstice** on **Nightal 20**

