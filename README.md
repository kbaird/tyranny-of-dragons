# [Tyranny of Dragons](https://www.dndbeyond.com/campaigns/2658268)

[Hoard of the Dragon Queen](https://www.dndbeyond.com/sources/hotdq) +
[Rise of Tiamat](https://www.dndbeyond.com/sources/rot)

## Forgotten Realms / Faerûn

[The year](http://dnd.steinhour.net/Forgotten_Realms_campaign/Forgotten_Realms/DnD_FR_calendar.html)
is apparently [1489 DR](https://forgottenrealms.fandom.com/wiki/1489_DR).

It starts in
[The Western Heartlands](https://www.dndbeyond.com/sources/scag/welcome-to-the-realms#TheSwordCoastandtheNorth),
just to the west of the
[Giant's Run Mountains](https://forgottenrealms.fandom.com/wiki/Giant%27s_Run_Mountains),
so [Gadi](PCs/Gadi/In-Character-Goals.md) has a few options for
[getting to Greenest](PCs/Gadi/Greenest.md).

### Maps
- [Interactive Map of Waterdeep](https://www.aidedd.org/atlas/index.php?map=W&l=1) (and other locales)
- [Map of Faerûn](https://loremaps.azurewebsites.net/Maps/Faerun) (3.5 map)
- [Sword Coast / North America Overlay](http://2.bp.blogspot.com/-5HsmtaGxbuk/VjLBkHtrgfI/AAAAAAAAN9I/rQl7-GbA__s/s1600/Faerun%2BGE%2BSnip%2B3.PNG)
- [Political map with hexes](https://i.imgur.com/7eGD1Er.jpg)
- [Interactive Map of Toril](https://www.dndcombat.com/toril/)
- [Updated Map of the Sword Coast](https://www.reddit.com/r/dndnext/comments/d3653q/updated_map_of_the_sword_coast_5e/)
- [Travel in the Roman Empire](https://orbis.stanford.edu/)
- [Human Ethnicities of Faerûn](https://imgur.com/a/Wtzdp2g)
