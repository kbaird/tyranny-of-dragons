# DATES
- c. -30K DR The **War of the Seldarine** begins. **Araushnee** is cast down
  into the **Demonweb Pits**, where she becomes the demon-goddess **Lolth**.
- c. -15K DR The first great kingdom of the dwarves of Faerûn is centered in
  the **Great Cavern of Bhaerynden**, deep beneath the **Shaar**.
- c. -12K DR The **First Crown War** begins.
- c. -11700 DR The **Second Crown War** begins.
- c. -10900 DR The **Third Crown War** begins.
- c. -10450 DR The **Fourth Crown War** begins.
- c. -9200 DR The **Fifth Crown War** begins.
- c. -9000 DR
  - The drow of **Telantiwar** overwhelm the dwarves of **Bhaerynden**.
  - The **Fifth Crown War** ends with the defeat of the **Vyshaantar** Empire.
- c. -8800 DR The Great Cavern of **Bhaerynden** collapses, creating the
  **Great Rift**.
- c. -7800 DR The djinni **Calim** founds the **Calim Empire** on the site of
  present-day **Calimport**.
- c. -7600 DR The dwarves return to their ancestral home, now the **Great Rift**.
- c. -5000 to -3200 DR The First Age of **Calimshan**
- -3983 DR The founding of **Cormanthor**
- c. -3859 to -3534 DR The First Age of **Netheril**
- -339 DR, The _Year of Sundered Webs_: The Fall of **Netheril**
- 261 DR, The _Year of Soaring Stars_: **Cormanthor** raises a _mythal_, becoming
  the multi-racial city of **Myth Drannor**.
- 714 DR, The _Year of Doom_: The Fall of **Myth Drannor**
- 1449-10-04 DR, The _Year of Godly Invitation_:
  **Gadi Melaku** born, 4 Marpenoth / Leaffall.
- 1454-01-17 DR, The _Year of the Emerald Sun_:
  **Kaleb Melaku** born, 17 Hammer / Deepwinter.
- 1467 DR, The _Year of the Three Heroes United_:
  **Zasheida Rein** born; Gadi graduates "high school".
  Perhaps **Xylem** born around this time as well?
- 1471 DR, The _Year of the Plagued Lords_: Gadi gets his B.M.S.
- 1474 DR, The _Year of the Fourth Circle_: Gadi gets his M.M.S. and enlists in
  the **Steel Shields** as an O-1 (2nd Lt.)
- 1482 DR, Gadi promoted to O-2 (1st Lt.)
- Mid-to-late 1480s DR, Zasheida serves in the Calishite army, posing as a man
  named **Zasheir**.
- 1489 DR, The _Year of the Warrior Princess_: Campaign begins

## Campaign start
- 1489-06-17: Campaign Start
- 1489-06-18: Next morning, aftermath of the attack on **Greenest**
- 1489-06-23: Get to **Candlekeep**
- 1489-06-27?: Reach the Grippli village
- 1489-06-30: **Gadi** writes to **Subira** from **Candlekeep**
- 1489-07-06: PCs depart **Candlekeep** on the _Dog Ear_, captained by
  **Mitor Jans**, bound for **Baldur's Gate**.
- 1489-07-08: PCs arrive in **Baldur's Gate** and get into a fight with the
  **Rotting Tongue Gang** in the sewers.
- 1489-07-09: PCs awaken in **Barovia**.
- 1489-07-10: The _Team Characterized by Dependably Stalwart Competence_ (TCbDSC)
  wakes up in our room at the **Blade and Stars Inn** in **Baldur's Gate**, having
  returned from a side excursion in **Barovia**. We now have a shattered remnant
  of one of the cult's masks that we believe are used to control dragons.
- 1489-07-31: 3 weeks to/in/from **Candlekeep**, then back in **Baldur's Gate**.
  - Various additional research and touristy things.
- 1489-08-15: The caravan suspected of harboring the Cult departs
  **Baldur's Gate**, headed north.
- [1489-08-23ish](Sessions/2023-01-08.md): We entered "The Fields of the Dead",
  rescued **Carlin** (the **Harper** who had been buried up to his head), and
  encountered the Myconids under the road. We made our way to a few days' travel
  south of **Waterdeep**.
- [1489-09-28](Sessions/2023-02-12.md):
  - **Carlin Emoffel** was revealed to be a double-agent. We helped **Jamna**
  rob the best items from the caravan loot, looked like Big Damn Heroes, and
  made our way into **Waterdeep**.
- [1489-12-04](Sessions/2023-03-05.md):
  - We make our way north along the High Road to a compound called the
  **Carnath Roadhouse** near the **Mere of Dead Men**. We befriend a Lizardfolk
  named **Snapjaw**, who tells us about the activities of the
  **"Dragonkneelers"**.
- [1489-12-1x](2023-04-30.md):
  - We investigate the barn and meet **Voaraghamanthar**, agreeing to work against
  the Cult. We are making our way to the hunting lodge and **Skyreach Castle**.
- [1490-01-01](2023-05-27.md):
  - We investigate the lodge, communicate through various _Sendings_, discover that
  the right portal goes to **Parnast**, the middle to **Thay**, and the left to a
  castle in the sky, which we follow.
- [1490-01-02](2023-07-22.md):
  - We arrive in the Ogre Barracks on **Skyreach** and sneak our way over to
  **Sandesyl**'s tower. She will arrange a meeting between us and **Blagothkus**,
  the Storm Giant who resents the presence of the Cult on **Skyreach**.
- [1490-01-03](2023-08-13.md):
  (late evening): We scoped out **Skyreach Castle** and met with **Blagothkus**,
  convincing him (despite **Gadi**'s involvement) to ally with us, mostly due to
  his fondness for **Radcliffe**. Tentative plans to help **Blagothkus** take
  control of **Skyreach** from both the Cult and **Glazhael** (the White Dragon),
  with him then being a future ally against the Cult. Various _Sendings_;
  **Voarag** is on his way, etc. **Blagothkus** plans to call some decoy meeting
  so we can sneak into **Rezmir**'s room to steal the Mask.
- [1490-01-04](2023-09-10.md):
  Next evening, we destroy the Black Dragon Mask and drive away **Glazhael**,
  while **Rezmir** and **Rathmodar** get away, sabotaging **Skyreach** such that
  it is falling. We need to survive and get to **Waterdeep**.
- [1490-01-08](2023-10-29.md):
  - The **Team Characterized by Dependably Stalwart Competence** saves **Skyreach**
  and returns to **Waterdeep** for debriefing. While there, the **Drakhorn** calls
  all Chromatic Dragons to war. We are given a guide and horses, and asked to meet
  a contact at **Boareskyr Bridge** to deal with **Neronvain**, who has been
  attacking Elves in the forest with some Draconic allies.
- [1490-01-20](2023-11-26.md):
  - The **Team Characterized by Dependably Stalwart Competence** makes their way to
  **Boareskyr Bridge**, guided by local Yuan-Ti to the **Tomb of Diderius**, which
  **Varram the White** has already entered with this barbarian mercenaries.
- [1490-01-20](2023-12-17.md):
  - The **Team Characterized by Dependably Stalwart Competence** encounters
  **Merrshaulk**-worshipping Yuan-Ti inside the **Tomb of Diderius**, tracking
  **Varram the White**.
- [1490-01-20](2024-01-21.md):
  - The **Team Characterized by Dependably Stalwart Competence** captures
  **Varram the White** from the **Tomb of Diderius**, bringing him to **Waterdeep**.
