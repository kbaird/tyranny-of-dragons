rm *.aux 2> /dev/null
rm *.log 2> /dev/null
rm **/*.aux 2> /dev/null
rm **/*.log 2> /dev/null

for FILE in *.tex; do
  latexindent -l -s -w $FILE
done

for FILE in **/*.tex; do
  latexindent -l -s -w $FILE
done

rm *.bak* 2> /dev/null
rm **/*.bak* 2> /dev/null
rm indent.log
