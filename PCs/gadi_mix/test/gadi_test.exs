defmodule GadiTest do
  use ExUnit.Case
  doctest Gadi

  # https://hexdocs.pm/propcheck/readme.html
  use PropCheck

  describe "average_damage" do
    property "returns a non-negative number from average_damage(:axe, ac, times)" do
      quickcheck(
        forall {ac, times} <- {pos_integer(), pos_integer()} do
          dam = Gadi.average_damage(:axe, ac, times)
          assert is_number(dam)
          assert dam >= 0.0
        end
      )
    end

    property "returns a non-negative number from average_damage(:bolt, ac, times)" do
      quickcheck(
        forall {ac, times} <- {pos_integer(), pos_integer()} do
          dam = Gadi.average_damage(:bolt, ac, times)
          assert is_number(dam)
          assert dam >= 0.0
        end
      )
    end

    property "returns a non-negative number from average_damage(:orb, ac, times)" do
      quickcheck(
        forall {ac, times} <- {pos_integer(), pos_integer()} do
          dam = Gadi.average_damage(:orb, ac, times)
          assert is_number(dam)
          assert dam >= 0.0
        end
      )
    end

    property "returns a non-negative number from average_damage(:sling, ac, times)" do
      quickcheck(
        forall {ac, times} <- {pos_integer(), pos_integer()} do
          dam = Gadi.average_damage(:sling, ac, times)
          assert is_number(dam)
          assert dam >= 0.0
        end
      )
    end
  end

  describe "pit_fighting" do
    test "returns a %Gadi.PitFighting{}" do
      result = Gadi.pit_fighting()
      assert is_struct(result, Gadi.PitFighting)
    end

    property "returns a list of {atom, boolean, int, int} tuples in :rounds" do
      quickcheck(
        forall _ <- pos_integer() do
          %{rounds: result} = Gadi.pit_fighting()
          assert is_list(result)
          refute Enum.empty?(result)

          all_asserts =
            for {label, success?, roll, dc} <- result do
              assert is_atom(label)
              assert is_boolean(success?)
              assert is_integer(roll)
              assert is_integer(dc)
              assert success? == roll >= dc
            end

          Enum.all?(all_asserts)
        end
      )
    end

    property "contains :complication" do
      complications = [
        "An opponent swears to take revenge on you.",
        "A crime boss approaches you and offers to pay you to intentionally lose a few matches.",
        "You defeat a popular local champion, drawing the crowd’s ire.",
        "You defeat a noble’s servant, drawing the wrath of the noble’s house.",
        "You are accused of cheating. Whether the allegation is true or not, your reputation is tarnished.",
        "You accidentally deliver a near-fatal wound to a foe."
      ]

      quickcheck(
        forall _ <- pos_integer() do
          %{complication: complication} = Gadi.pit_fighting()
          assert is_nil(complication) or complication in complications
        end
      )
    end

    property "contains :earnings" do
      earnings_by_count = [0, 50, 100, 200]

      quickcheck(
        forall _ <- pos_integer() do
          %{earnings: earnings, rounds: rounds} = Gadi.pit_fighting()
          assert earnings in earnings_by_count

          successes =
            rounds
            |> Enum.filter(&success?/1)
            |> length()

          assert earnings == Enum.at(earnings_by_count, successes)
        end
      )
    end
  end

  defp success?(results) do
    Keyword.get(results, :success?, false)
  end
end
