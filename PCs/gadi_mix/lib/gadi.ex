defmodule Gadi do
  @moduledoc false

  @max_flow_demand 50

  @to_hit_bonus %{
    axe: 2,
    bolt: 9,
    orb: 9,
    sling: 4
  }

  @weapons Map.keys(@to_hit_bonus)

  defmodule PitFighting do
    @moduledoc "Encloses PitFighting results"
    defstruct ~w[complication earnings rounds]a
  end

  defguardp is_pos(arg) when is_integer(arg) and arg > 0

  @doc "Returns the average damage for a given weapon vs. a given AC"
  def average_damage(weapon, ac, times \\ 999_999)
      when weapon in @weapons and is_pos(ac) and is_pos(times) do
    1..times
    |> Flow.from_enumerable(max_demand: @max_flow_demand)
    |> Flow.map(fn _ -> damage(weapon, ac) end)
    |> Enum.sum()
    |> Kernel./(times)
  end

  def d(die_type, cnt \\ 1)

  def d(die_type, 1) when is_pos(die_type) do
    Enum.random(1..die_type)
  end

  def d(die_type, cnt) when is_pos(die_type) and is_pos(cnt) do
    Enum.map(1..cnt, fn _ -> "#{Enum.random(1..die_type)}" end)
  end

  def pit_fighting do
    rounds = Enum.map(pit_fighting_bonuses(), &pit_fight/1)

    struct(__MODULE__.PitFighting, %{
      complication: complication(),
      earnings: earnings(rounds),
      rounds: rounds
    })
  end

  ### PRIVATE FUNCTIONS

  defp complication do
    case d(10) do
      1 ->
        complications = [
          "An opponent swears to take revenge on you.",
          "A crime boss approaches you and offers to pay you to intentionally lose a few matches.",
          "You defeat a popular local champion, drawing the crowd’s ire.",
          "You defeat a noble’s servant, drawing the wrath of the noble’s house.",
          "You are accused of cheating. Whether the allegation is true or not, your reputation is tarnished.",
          "You accidentally deliver a near-fatal wound to a foe."
        ]

        complications |> Enum.shuffle() |> hd()

      _ ->
        nil
    end
  end

  defp damage(:orb) do
    Enum.random(1..8) + Enum.random(1..8) + Enum.random(1..8)
  end

  defp damage(:sling), do: Enum.random(1..4) + 2

  defp damage(weapon) when weapon in ~w[axe bolt]a, do: Enum.random(1..10)

  defp damage(weapon, ac) do
    if hit?(weapon, ac), do: damage(weapon), else: 0
  end

  defp dc do
    d(10) + d(10) + 5
  end

  defp earnings(rounds) do
    successes = rounds |> Enum.filter(&success?/1) |> length()
    Enum.at([0, 50, 100, 200], successes)
  end

  defp hit?(weapon, ac) do
    to_hit = d(20) + @to_hit_bonus[weapon]
    to_hit >= ac
  end

  defp pit_fight({label, bonus}) when is_atom(label) and is_pos(bonus) do
    roll = d(20) + bonus
    dc = dc()
    [task: label, success?: roll >= dc, roll: roll, dc: dc]
  end

  defp pit_fighting_bonuses do
    [
      {:acrobatics, 2},
      {:attack, 5},
      {:con, 2 + d(4)}
    ]
  end

  defp success?(result) do
    Keyword.get(result, :success?, false)
  end
end
