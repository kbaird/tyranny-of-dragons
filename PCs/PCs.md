# PCs

We're hoping to have a long-running campaign because we want to get to fairly
high levels. Mo gets to ride her drake at 15th level, for example.

## [Radcliffe Carpenter](https://www.dndbeyond.com/characters/68288403) (Phil)

"[Human](https://www.dndbeyond.com/races/human)"
[Druid](https://www.dndbeyond.com/classes/druid) /
[Warlock of the Archfey](https://www.dndbeyond.com/classes/warlock)
who is actually a shape-changed fox.

UC Santa Cruz hippie / **Radagast**-type Druid. Just travelling through
**Greenest** at the moment, but open to joining others.

Red-headed mountain man. Lives up in the hills, camping out. Has been recruited
into the **Emerald Enclave**.

## [Gadi Melaku 稳 of the Ayhudi](Gadi/In-Character-Goals.md) (Kevin)

[Gold Dwarf](https://www.dndbeyond.com/sources/scag/races-of-the-realms#GoldDwarves)
[Order of Scribes Wizard](https://www.dndbeyond.com/classes/wizard#OrderofScribes)
from
[The Great Rift](https://forgottenrealms.fandom.com/wiki/Great_Rift).

[Starting in Greenest](../PCs/Gadi/Greenest.md)

Is a family friend of **Zasheida**, and has met up with her in **Greenest**
before the campaign starts.

## [Zasheida Rein](Zasheida-Rein.md) (Morgan)

[Human](https://www.dndbeyond.com/races/human)
[Drakewarden Ranger](https://www.dndbeyond.com/classes/ranger#Drakewarden)
from
[Calimshan](https://forgottenrealms.fandom.com/wiki/Calimshan).

Running from the **Calishite** Army because her deception of pretending to be a
man was discovered.

## [Xylem](https://www.dndbeyond.com/characters/69336669) (Paul)

[Half-Elf](https://dndbeyond/races/half-elf)
[Rogue](https://dndbeyond/classes/rogue) /
[Monk](https://dndbeyond/classes/monk)
[Investigator](https://www.dndbeyond.com/backgrounds/investigator)

Born to a clan of Druids under auspicious circumstances, he is a fraternal twin
to his sister **Phloem**. The problem is that **Xylem** has no Druidic ability
at all. He was instead sent to an affiliated monastery.

Quite intentionally non-descript, and just arriving in **Greenest** as we start.
Has been recruited into the **Emerald Enclave**.

## Roles
- Front Line (Radcliffe, Xylem, Zasheida)
- Beat Down (Zasheida via bow, Xylem, Radcliffe, Gadi)
- Utility (Gadi, Radcliffe, Zasheida)
- Support (Radcliffe, Zasheida, Gadi)
- Investigator (Xylem, Gadi, Radcliffe)
- Negotiator (Zasheida, Radcliffe)
  - Gadi in a pinch at 3rd level with
  [Borrowed Knowledge](https://www.dndbeyond.com/spells/borrowed-knowledge)
- Infiltrator / Explorer (Xylem, Radcliffe)
  - (Gadi, Zasheida) via Familiar, Drake, and
  [Manifest Mind](Gadi/Manifest-Mind.md)
