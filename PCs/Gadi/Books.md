# Gadi's Books

- _Beers of Faerûn: From Riftic Stout to Inferior Foreign Brews_,
  written in Dwarvish/Dethek

- _Mycontil's Shadow_,
  A mildly revisionist biography of the **Halruaan** Archmage
  [Mycontil](https://forgottenrealms.fandom.com/wiki/Mycontil)
  which suggests that Mycontil's success owed much to a
  traditionally-overlooked assistant/advisor,
  written in Halruaan/Draconic

- _A survey of Great Rift stonemasonry techniques in the context of
  6th Century institutional architecture_,
  written in Dwarvish/Dethek

- _Firithmuin_ (Sindarin for "Fading Family"), an epic poem about the
  **Crown Wars**, written in Elvish/Espruar

