# Melaku's Vanguard

_Staff, unique (requires attunement by a Wizard)_

A staff, comfortably sized for a Dwarf, and made out of sleek charcoal-colored
metal. Embossed just-barely glowing reddish-burgundy Mithral etchings wrap
across its surface with writings in Dethek runes exhorting the bearer to protect
civilians and act in defense of The People. It feels heavier in the hand than its
literal weight as measured by a scale, as if a literal manifestation of a
profound moral burden.

You gain a +2 bonus to attack and damage rolls made with this magic quarterstaff.
While you hold it, you gain a +2 bonus to spell attack rolls.

The staff has 10 charges. While holding it, you can use an action to expend 1
or more of its charges to cast one of the following spells from it, using your
spell save DC and spellcasting ability:
- _Mage Armor_ (1 charge)
- _Shield_ (1 charge)
- _Stoneskin_, skinned as metallic rather than stone (4 charges)
- _Summon Elemental_, Earth only (4 charges)
- _Conjure Elemental_, Earth only (5 charges)

The staff regains 1d6+4 expended charges daily at dawn. If you expend the last
charge, roll a d20. On a 1, the staff starts to rust at an alarming rate,
becoming useless.

