# Melaku's Dancing Dagger of Defense

_Weapon (dagger), very rare (requires attunement)_

A variant of the
[Dancing Sword](https://www.dndbeyond.com/magic-items/5383-dancing-sword) that
dances around **Zasheida** to protect her from attacks (perhaps any, perhaps
only melee), imposing _Disadvantage_ on the first such attack against her each
round. It would still cease operation in the same manner after functioning four
times.

## Old Idea

### Melaku's Righteous Rebuke

Maybe think of a different item. **Zasheida** already has the
**Longbow of the Emerald Enclave**.

_Weapon (longbow), unique (requires attunement by a Ranger)_

An anachronistically modern-looking compound longbow, sized for a medium-small
Human, made out of sleek charcoal-colored metal. Embossed just-barely glowing
reddish-burgundy Mithral etchings wrap across its surface with writings in Dethek
runes exhorting the bearer to protect civilians and act in defense of The People.
It feels heavier in the hand than its literal weight as measured by a scale, as
if a literal manifestation of a profound moral burden.

It grants +2 to attack and damage rolls. If the attack is made with Advantage,
it also does an additional 1d6 thunder damage.

