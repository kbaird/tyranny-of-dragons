# Gadi Magic Items Wishlist

As an [Order of Scribes](https://www.dndbeyond.com/classes/wizard#OrderofScribes)
Wizard, perhaps he could learn to inscribe himself with a
[Barrier Tattoo](https://www.dndbeyond.com/magic-items/barrier-tattoo) of his
personal Wizard sigil of [稳](https://en.wiktionary.org/wiki/%E7%A9%A9)
("wěn": stable; firm; solid; steady). That could also increase in power over
time.

Staves are awesome:
- [Staff of Power](https://www.dndbeyond.com/magic-items/staff-of-power)
- [Staff of the Magi](https://www.dndbeyond.com/magic-items/staff-of-the-magi)
- [Voyager Staff](https://www.dndbeyond.com/magic-items/voyager-staff)

A [Carpet of Flying](https://www.dndbeyond.com/magic-items/carpet-of-flying) or
[Pearl of Power](https://www.dndbeyond.com/magic-items/pearl-of-power) would
also be nice.

I would also like
[items that improve saving throws](https://www.dndbeyond.com/magic-items?filter-type=0&filter-effect-type=1&filter-effect-subtype=8)

If in **Candlekeep** or a similar library, get
[formulae](https://www.dndbeyond.com/sources/xgte/downtime-revisited#CraftinganItem)
for
- ~https://www.dndbeyond.com/magic-items/bracers-of-defense~
  (maybe not in conjunction with the tattoo)
- https://www.dndbeyond.com/magic-items/ring-of-warmth
  (especially if in the cold North a lot)
- https://www.dndbeyond.com/magic-items/sending-stones
  (possibly flavored as metallic for smithing)

## Other metallic craftable items
(in addition to anything above)
- https://www.dndbeyond.com/magic-items/5420-ring-of-resistance
- https://www.dndbeyond.com/magic-items/ring-of-feather-falling
- https://www.dndbeyond.com/magic-items/ring-of-spell-storing
- https://www.dndbeyond.com/magic-items/ring-of-temporal-salvation

## Other misc items
- https://www.dndbeyond.com/magic-items/arcane-grimoire
- https://www.dndbeyond.com/magic-items/atlas-of-endless-horizons
- https://www.dndbeyond.com/magic-items/skyblinder-staff

I would like Gadi to craft signature magic items made of metal, most likely a
[unique metal staff](MV.md) for himself and
[a sleek compound bow](MRR.md) for Zasheida.

