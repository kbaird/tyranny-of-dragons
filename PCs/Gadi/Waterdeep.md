**Gadi** wants to visit the **Snobeedle Orchard and Meadery** on _Underclff Way_
to the east of **Waterdeep**.

# Potential places to rent a room in **Waterdeep**
- **Gondalim's** [_CoS:WD_, pg100, T8], an inn at the corner of
  **The High Road**, **Winter Path**, and **Burnt Wagon Way**
  (just west of the **City of the Dead**).
- **Inn of the Dripping Dagger** [_CoS:WD_, pg100, T3], 2 blocks north
- **The Unicorn's Horn** [_CoS:WD_, pg100, T15], corner of **The High Road**
  and **Waterdeep Way**. Having to walk all the way up to the 6th floor might
  be cheaper.

## Cheaper places
- **Warm Beds** [_CoS:WD_, pg104, D15] at the corner of **Ship Street** and
  **Presper Street** in the **Dock Ward**.
- **Safehaven Inn** [_CoS:WD_, pg105, S41] in the **Southern Ward**.

He also knows **Jalarra Sakrel** [_CoS:WD_ pg24], a graduate student at the
**Blackstaff Academy** in the NW of the _Castle Ward_ of **Waterdeep**.
