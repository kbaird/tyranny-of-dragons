# Gadi Homebrew Spells

(Usually self-created)

**Delay Next Spell**, allows the next spell cast to take effect (and start
requiring concentration) after a specified duration.

## Cantrip
- https://www.dndbeyond.com/spells/903178-melakus-force-bolt

## Level 1
- https://www.dndbeyond.com/spells/3785-image-capture
- https://www.dndbeyond.com/spells/883396-melakus-portal-beacon
- https://www.dndbeyond.com/spells/899429-melakus-singular-deception
- ~https://www.dndbeyond.com/spells/902233-melakus-detect-enemies~
- https://www.dndbeyond.com/spells/940511-melakus-grooming
- https://www.dndbeyond.com/spells/956447-melakus-disguise-ally
- https://www.dndbeyond.com/spells/971699-melakus-locate-self

## Level 2
- https://www.dndbeyond.com/spells/214075-confidential-conversation
- https://www.dndbeyond.com/spells/876562-melakus-protection-from-charm
- https://www.dndbeyond.com/spells/878177-melakus-subtle-summons
- https://www.dndbeyond.com/spells/879084-melakus-improved-floating-disk
- https://www.dndbeyond.com/spells/965748-melakus-cart

## Level 3
- https://www.dndbeyond.com/spells/883435-melakus-analyze-portal

## Level 4
- https://www.dndbeyond.com/spells/867387-melakus-locate-portal
- https://www.dndbeyond.com/spells/900104-melakus-sprightly-glyph

## Variants
- https://www.dndbeyond.com/spells/encode-thoughts that's permanent and higher-level
