# Gadi Spell Wishlist

Spells can be copied into my spellbook for 50GP / spell level. It normally takes
2 hours / spell level, but the **Wizardly Quill** reduces hours -> minutes.

[Homebrew](Homebrew.md)

## Level 1
- [Charm Person](https://www.dndbeyond.com/spells/charm-person)
- [Color Spray](https://www.dndbeyond.com/spells/color-spray)
- [Disguise Self](https://www.dndbeyond.com/spells/disguise-self)
- [Ice Knife](https://www.dndbeyond.com/spells/ice-knife)
- [Illusory Script](https://www.dndbeyond.com/spells/illusory-script)
- [Protection from Evil and Good](https://www.dndbeyond.com/spells/protection-from-evil-and-good)
- [Shield](https://www.dndbeyond.com/spells/shield)
- [Silvery Barbs](https://www.dndbeyond.com/spells/silvery-barbs)
- [Tasha's Hideous Laughter](https://www.dndbeyond.com/spells/tashas-hideous-laughter)
- [Tenser's Floating Disk](https://www.dndbeyond.com/spells/tensers-floating-disk)
- [Unseen Servant](https://www.dndbeyond.com/spells/unseen-servant)

## Level 2
- [Dragon's Breath](https://www.dndbeyond.com/spells/dragons-breath), Cast it on Gwadenya
- [Gentle Repose](https://www.dndbeyond.com/spells/gentle-repose)
- [Phantasmal Force](https://www.dndbeyond.com/spells/phantasmal-force)
- [Tasha's Mind Whip](https://www.dndbeyond.com/spells/tashas-mind-whip)
- [x] [Vortex Warp](https://www.dndbeyond.com/spells/vortex-warp)
- [Web](https://www.dndbeyond.com/spells/web)
- [Wither and Bloom](https://www.dndbeyond.com/spells/wither-and-bloom)
  - Healing spell available to Wizards, although Phil is taking it

## Level 3
- [x] [Counterspell](https://www.dndbeyond.com/spells/counterspell)
- [Dispel Magic](https://www.dndbeyond.com/spells/dispel-magic)
- [x] [Enemies Abound](https://www.dndbeyond.com/spells/enemies-abound)
- [Hypnotic Pattern](https://www.dndbeyond.com/spells/hypnotic-pattern)
- [Leomund's Tiny Hut](https://www.dndbeyond.com/spells/leomunds-tiny-hut)
- [x] [Magic Circle](https://www.dndbeyond.com/spells/magic-circle)
- [Phantom Steed](https://www.dndbeyond.com/spells/phantom-steed)
- [Speak With Dead](https://www.dndbeyond.com/spells/speak-with-dead)
- [Spirit Shroud](https://www.dndbeyond.com/spells/spirit-shroud)
  - Grants radiant, necrotic, cold
- [Thunder Step](https://www.dndbeyond.com/spells/thunder-step)
- [Tidal Wave](https://www.dndbeyond.com/spells/tidal-wave)
  - Grants bludgeoning
- [Water Breathing](https://www.dndbeyond.com/spells/water-breathing)

## Level 4
- [x] [Banishment](https://www.dndbeyond.com/spells/banishment)
- [Charm Monster](https://www.dndbeyond.com/spells/charm-monster)
- [x] [Dimension Door](https://www.dndbeyond.com/spells/dimension-door)
- [x] [Galder's Speedy Courier](https://www.dndbeyond.com/spells/galders-speedy-courier)
- [x] [Locate Creature](https://www.dndbeyond.com/spells/locate-creature)
- [Polymorph](https://www.dndbeyond.com/spells/polymorph)
- [x] [Stoneskin](https://www.dndbeyond.com/spells/stoneskin)
- [x] [Summon Elemental](https://www.dndbeyond.com/spells/summon-elemental)

## Level 5
- [x] [Animate Objects](https://www.dndbeyond.com/spells/animate-objects)
- [x] [Contact Other Plane](https://www.dndbeyond.com/spells/contact-other-plane)
- [Dominate Person](https://www.dndbeyond.com/spells/dominate-person)
- [Modify Memory](https://www.dndbeyond.com/spells/modify-memory)
- [Planar Binding](https://www.dndbeyond.com/spells/planar-binding)
  - Have [Radcliffe](Radcliffe-Carpenter.md) cast
  [Summon Elemental](https://www.dndbeyond.com/spells/summon-elemental) and
  maintain control while Gadi binds it. Upcasting to a 6th-level slot increases
  the duration from 1 day -> 10 days, and Elementals have low CHA saves. Presto,
  small army.
- [Rary's Telepathic Bond](https://www.dndbeyond.com/spells/rarys-telepathic-bond)
  - Miss Martian link for 1 hour
- [Synaptic Static](https://www.dndbeyond.com/spells/synaptic-static)
- [x] [Teleportation Circle](https://www.dndbeyond.com/spells/teleportation-circle)
- [Wall of Force](https://www.dndbeyond.com/spells/wall-of-force)

## Level 6
- [Contingency](https://www.dndbeyond.com/spells/contingency)
  - Whenever I involuntarily fail a save against a magical effect that produces
  a Condition, cast
  [Dispel Magic](https://www.dndbeyond.com/spells/dispel-magic).

## Level 7
- [Force Cage](https://www.dndbeyond.com/spells/force-cage)
