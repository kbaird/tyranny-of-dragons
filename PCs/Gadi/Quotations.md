# Gadi Quotations

> I am Loremaster Gadi Melaku of the Ayhudi clan of the Great Rift, Spellsmith,
> Master of Magical Sciences, son of Tamara and Abel, brother of Kaleb, father of
> Noriess, second of that name, husband of the incomparable Subira Tesfaye,
> priestess of the great Moradin, Soul Forger and father of all Dwarfkind.
- How may I be of assistance?
- ...and you are sitting in my seat.

> Pitter patter, let's get at her.

> Healthy body; healthy mind.

> It's just as easy to have a nice, crisp uniform as not.

> You're expecting iron to bend like gold
> Expecting me to pick a lock with my beard

> Like folding stone

> I expect to die here, atop a large pile of my enemies' corpses.

> What would my ancestors say?

> **Baruk Khazâd! Khazâd ai-mênu!**:
> "Axes of the Dwarves! The Dwarves are upon you!"

> Listen, human: If you think I'm going to bring shame to countless generations
> of my ancestors by backing down from a measly little threat from you, you are
> woefully mistaken.

> Steel and stone and rune and fire.
> Gold and dark and blood and fire.
> Heart and soul and beer and fire.
> These things make a dwarf.
- Ancient dwarven song, used to keep time when pumping bellows
  (from 13th Age _Book of Loot_)

> The Great Rift doesn't have an army. It is an army.

> I don't generally use weapons. I am a weapon.

> Are there any human towns that are actual communities?

> <sarcasm>
> Look at me! I'm a human doing whatever I think is best, regardless of what
> the clan elders say!
> </sarcasm>

> Of course I'm depressed. I'm thousands of miles from my people.

> As the size of an explosion increases, the number of social situations it is
> incapable of solving approaches zero.
— **Vaarsuvius** from _The Order of the Stick_

> A  lot of people are going to get hurt tomorrow. All we can do is stand in
> the way of that and say, "Not them. Me. If you need to hurt someone, hurt me."
> ... Because the alternative is to look at someone else, someone weaker and
> more vulnerable, and tell them that you want them to be hurt instead of you.
— **O-Chul** from _The Order of the Stick_

> Being a dwarf is all about doing your duty, even if it makes you miserable.
> _Especially_ if it makes you miserable.

## Insults

> He's a couple beers short of a breakfast.

> For sale: military uniform. No sweat stains.

- _Mosgrim_ ("Beardless") lackwit
  - Note that women are _thalaknich_ ("clear-chinned"), which is not an insult.
- Ignoramus McRunshismouth
- Baron Simpleton von Lackwit, of the _Waterdeep_ dullards
