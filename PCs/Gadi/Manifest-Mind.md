# Gadi Manifest Mind

Takes the form of
[**Noriess**, the "Mother of Mithral"](https://www.heroforge.com/load_config%3D16659110/),
a pioneering Gold Dwarf Wizard from thousands of years ago who developed the
variant of _Dethek_ used for arcane runes, similar to
[Sejong the Great](https://en.wikipedia.org/wiki/Sejong_the_Great)'s
creation of [한글](https://en.wikipedia.org/wiki/Hangul).
