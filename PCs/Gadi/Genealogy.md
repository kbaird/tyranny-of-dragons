# Gadi Genealogy

Gadi (Wizard) and Kaleb (Brewer and Merchant) Melaku
- Tamara Alemu ♀ (Colonel in the _Steel Shields_)
  - Liya Tedros ♀
    - Aida Birhane ♀
    - Yonas Tedros ♂
  - Ahmed Alemu ♂ (Soldier)
    - Nyala Deresse ♀
    - Kofi Alemu ♂
- Abel Melaku ♂ (Metalsmith)
  - Zala Kahinu ♀ (Metalsmith)
    - Marjani Teshome ♀
    - Semere Kahinu ♂
  - Yelekal Melaku ♂ (Rancher)
    - Ife Dawit ♀ (Rancher)
    - Tebeb Melaku ♂ (Rancher)

**Abel**'s sister **Belkis** still runs the **Melaku** ranch inside the
political borders of the **Great Rift**'s territory, but on the surface.

**Gadi**'s childhood friend **Diarra Hailu** is now **Abel**'s assistant.

**Kaleb** brews _Riftic Stout_ and _Underhome Black Cherry Lambic_.

## Other NPCs from the Rift

### Almaz Mulu

Military historian, mutually respectful professional rival of **Tamara**.

### Haile Tesfaye

**Subira**'s father. Politician.
