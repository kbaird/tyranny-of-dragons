# Gadi Goals

See also [Rules Plans](Rules-Plans.md),
[Magic Items Wishlist](Magic-Items/Wishlist.md),
and [Spells Wishlist](Spells/Wishlist.md)

1. Be seen as "successful" in the eyes of critical future in-laws
  - Earn a name / title: "Spellsmith", "Loremaster", "Doctor of Thaumatology"
  - Do some Big Damn Hero stuff
    - Whenever he does something heroic during the campaign, he should ask a
  suitable authority figure to write to **Diarra** detailing his contributions.
  - Gather wealth
  - Improve the reputation of Gold Dwarves among the gentiles
  (maybe [join](https://www.dndbeyond.com/backgrounds/faction-agent) the
  [Harpers](https://forgottenrealms.fandom.com/wiki/Harpers))
2. Be able to create and research spells
3. Play into tropes for both Dwarves and Wizards and try to make them work together
4. Be able to deal with
  [portals](http://www.wizards.com/dnd/article.asp?x=fr/pg20020327x,0).
  I think they're cool.
5. Eventually get a magic carpet
6. Be a LG fish out of water
   (Moved to the bottom, as it might be a slight **Bakari** retread.)

He eventually wants to return to the **Great Rift**, marry **Subira Tesfaye**,
have a bunch of kids and a nice house or apartment, and maybe teach at the
**Academy Arcane**... although it might be interesting to explore reverse
culture shock.

Niche
- Smart / bookish guy
- Flexible utility caster
- Blaster / Battlefield Control in fights

## [Faerûn Astrology](https://www.realmshelps.net/charbuild/bday.shtml)

- **Age**: 40 in 1489
  (Dwarves are considered young until 50. **Gadi** will not volunteer this info.)
- **Born**: 4 Marpenoth(10), 1449 DR.
- **Year of Godly Invitation**
  Born under the **Sign of the Fish**, and with Disseminating **Selûne** under
  the **Sign of the Spear**.

Those born under the sign of the Fish are artistic. They have lively, analytical
minds and make inspiring teachers. Imaginative, they are radical and idealistic
thinkers.

Those born with Selûne under the sign of the Spear are practical, capable and
steadfast in adversity, cautious, logical and efficient. They have good business
sense but prefer to assist rather than lead. In relationships they are supportive,
protective and possessive.
