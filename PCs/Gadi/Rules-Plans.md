# Gadi Plans

For [the Great Rift](https://www.realmshelps.net/region/Great_Rift~Dwarf), it suggests
**Giant, Gnome, Goblin, Shaaran, Terran, Untheric** for Bonus Languages.

Wizard Sigil: [穩 / wěn](https://en.wiktionary.org/wiki/%E7%A9%A9#Chinese)

## [Order of Scribes](https://www.dndbeyond.com/classes/wizard#OrderofScribes)

See also [Wizard
101](https://www.dndbeyond.com/posts/959-wizard-101-order-of-scribes-from-tashas-cauldron)

Make spell scrolls, especially 1st or other low level spells that are extremely
useful in rare situations and not rituals. That way, I can still have access to
them even while preparing other spells. Also a potentially useful source of
income. (Gets a boost at level 10 with **Master Scrivener**).

- [Spells](Spells/Wishlist.md)
- [Magic Items Wishlist](Magic-Items/Wishlist.md)
- [Beers](Beers.md)
- [Politics](Politics.md)
- [Manifest Mind](Manifest-Mind.md)

## Level 3

Use
[Cantrip Formulas](https://www.dndbeyond.com/classes/wizard#OptionalClassFeatures)
to swap
[Melaku's Force Bolt](https://www.dndbeyond.com/spells/903178-melakus-force-bolt)
for [Fire Bolt](https://www.dndbeyond.com/spells/fire-bolt) if we expect Trolls
(and if it's not objectionable metagaming).

## Level 4
- +2 to Int (to 18)
- Pick another Cantrip, probably
  [Minor Illusion](https://www.dndbeyond.com/spells/minor-illusion), especially
  if it can be used to display what I see through my familiar and/or
  [Manifest Mind](Gadi-Manifest-Mind.md)

## Level 8
- +2 to Int (to 20)

## Level 10
- Pick another Cantrip, probably
  [Shape Water](https://www.dndbeyond.com/spells/shape-water)

## Level 12/16/19
- +2 to Con, Wis, or Dex
- [Tough](https://www.dndbeyond.com/feats/tough)
- [Linguist](https://www.dndbeyond.com/feats/linguist)
  - Languages: Primordial, Orcish, Giant, Goblin, Halruaan, Alzhedo, Ancient Netherese
- [Observant](https://www.dndbeyond.com/feats/observant)
- [Elemental Adept](https://www.dndbeyond.com/feats/elemental-adept),
  maybe **Thunder**, Cf. https://youtu.be/tCsU54cXDOE?t=1058, although **Fire**
  makes sense, since the feat cuts through resistance, and Fire is very common
- ~[Skill Expert](https://www.dndbeyond.com/feats/skill-expert)~
  (maybe no longer needed with [Xylem](PCs.md#Xylem) in the party)
  - Arcana or Religion for Expertise
  - Intimidation, Nature, or Investigation for Proficiency
