# Gadi Beers

- Riftic Stout
  - Brewed by his brother **Kaleb**
- https://forgottenrealms.fandom.com/wiki/Garlraw
- https://forgottenrealms.fandom.com/wiki/Muquet
- https://forgottenrealms.fandom.com/wiki/Thannaberry
- https://forgottenrealms.fandom.com/wiki/Amberfire
- Halruaan Pale Ale
- Hanseath's Amber Lager
- Habesha Cold Gold Pale Lager
- Underhome Black Cherry Lambic
  - Brewed by his brother **Kaleb**

(From Neverwinter Nights, Docks, Seedy Tavern, drinking contest with Jalek)
- Neverwinten Sailor Spirit
- Dockside Dunn's
- Orc Blood Brew
- Dwarven Red Eye
- Thayvian Fire Juice
- Catoblepas Death Cheese Wine

"Grackle" is a porridge of wheat, rice, oats, millet, or whatever is available,
combined with raisins, diced green apples, crushed almonds, and dusted with
cinnamon for flavor.
