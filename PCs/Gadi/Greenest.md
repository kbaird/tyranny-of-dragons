# Gadi getting to Greenest

Travel to the **Giant's Run** colony via
[portal](https://www.dndbeyond.com/spells/teleportation-circle), and then take
the [Trader's Road](https://forgottenrealms.fandom.com/wiki/Trader%27s_Road) W
to [Iriaebor](https://forgottenrealms.fandom.com/wiki/Iriaebor), a river boat
or raft W on the
[Chionthar](https://forgottenrealms.fandom.com/wiki/Chionthar), and then the
[Uldoon Trail](https://forgottenrealms.fandom.com/wiki/Uldoon_Trail) SW to
[Greenest](https://forgottenrealms.fandom.com/wiki/Greenest).

Perhaps meeting [Zasheida](Zasheida.md) there.
