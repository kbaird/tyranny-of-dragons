# Calishites
Shorter and slighter in build than most other humans, Calishites have dusky brown
skin, hair, and eyes. They're found primarily in southwest Faerûn.

**Calishite Names:**
- (Male) Aseir, Bardeid, Haseid, Khemed, Mehmen, Sudeiman, _Zasheir_;
- (Female) Atala, Ceidil, Hama, Jasmal, Meilil, Seipora, Yasheira, _Zasheida_;
- (Surnames) Basha, Dumein, Jassan, Khalid, Mostana, Pashar, _Rein_
