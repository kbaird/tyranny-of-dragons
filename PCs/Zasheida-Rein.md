# Zasheida ("Zasheir") Rein (Morgan)
- _Mulan_ Expy from [Calimshan](https://forgottenrealms.fandom.com/wiki/Calimshan)
  - [Drakewarden Ranger](https://www.dndbeyond.com/classes/ranger#Drakewarden)
    - [Gadi](Gadi/In-Character-Goals.md) is a family friend who knew her
      grandparents.

Her Drake companion is named **Nurril**.

The [Calishite](Calishites.md) Humans have recently [overthrown their Genasi
masters](https://www.dndbeyond.com/sources/scag/welcome-to-the-realms#LandstotheSouth)
That could've been the military action that she needed to masquerade for.

**Calimshan** also has many [Tethyrians](Tethyrians.md).

## [Faerûn Astrology](https://www.realmshelps.net/charbuild/bday.shtml)

- **Age**: 22 in 1489
- **Born**: 18 Uktar(11), 1467 DR.
- **Year of Three Heroes United**
  Born under the **Sign of the Butterfly**, and with New **Selûne** under
  the **Sign of the Eagle**.

Those born under the sign of the Butterfly are restless, sociable and good
natured. Cheerful, expansive and magnetic, they win friends easily and dislike
offending others. Although often indecisive, they are not weak willed and tackle
difficult tasks with infectious optimism.

Those with Selûne under the sign of the Eagle are determined, resilient and ambitious.
Good organizers, leaders and strategists, they are not deterred by setbacks, believing
hard work, patience and persistence will triumph. They are loyal but reserved in showing
affection.
