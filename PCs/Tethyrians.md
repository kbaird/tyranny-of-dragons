# Tethyrians
Tethyrians are of medium build and height, although taller and broader in build than
most Calishites. Their skin tends to have a dusky hue, although on average they are
increasingly fairer in complexion the farther north one travels along the Sword Coast,
reflecting a decreasing fraction of Calishite heritage and an increasing fraction of
Illuskan and Low Netherese ancestry. Tethyrian hair and eye color varies widely, with
brown hair and blue eyes being most common.

"In Calimshan, Tethyrians compose the bulk of the lower classes and have long been
discriminated against the largely Calishite upper classes." [_Races of Faerûn_, pg102].

**Tethyrian Names:**
- Feminine names. Arveene, Esvele, Jhessail, Kerri, Lureene, Miri, Rowan, Shandri, Tessele.
- Masculine names. Darvin, Dorn, Evendur, Gorstag, Grim, Helm, Malark, Morn, Randal, Stedd.
- Family names. Amblecrown, Buckman, Dundragon, Evenwood, Greycastle, Tallstag.
