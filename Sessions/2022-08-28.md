# 2022-08-28 Session, Session 4

## From previous sessions

- [2022-04-17](2022-04-17.md)
  - We all gathered in **Greenest** while the **Cult of the Dragon** attacked
- [2022-05-21](2022-05-21.md)
  - **Escobert** dueled, and we took off after a wounded **Langdedrosa**
- [2022-05-28](2022-05-28.md)
  - Follow **Langdedrosa** & go to the camp, rescuing the prisoners and stealing
  the Dragon eggs.
- [2022-06-26](2022-06-26.md)
  - Met Gnomes on the way to **Candlekeep**, accepted the job to look into the
  **Grippli** village.

### TODO / Setup

Look into the **Yuan-Ti** takeover of the coastal **Grippli** village near the
**Cloakwood** between **Candlekeep** and **Baldur's Gate**. The **Grippli** had
been supplying **Candlekeep** with food and other sea-based products. This trade
has stopped, so **Candlekeep** asked us to look into it (and hopefully solve it).

- Cylinder1: Getting skilled at farming crabs
- Cylinder2: Yuan-Ti arrive and takeover
- Cylinder3: Aftermath

**Pelk** believes that there is an ancient **Yuan-Ti** temple and settlement in
the area, so the **Yuan-Ti** may have a legitimate claim to the land.

## Swag

## Events of this session

We ride with **Cpt. Mitor Jans** (m Half-Orc, jovial)'s ship the _Dog Ear_ to
the settlement. **Radcliffe** and **Xylem** get seasick, but not too bad.
**Xylem** makes a concerted effort to assimilate and get some "sea legs";
**Radcliffe** just tries to get through it.

**Xylem** happens to be in the crow's nest, and sees a big shape in the water
approaching us. It's a squall. As it hits, **Gadi** and **Radcliffe** are saved
by **Xylem** and **Zasheida**. No injuries or damage to the ship. **Xylem** hears
the cracking of wood. He can't identify in greater detail. **Radcliffe** sees one
of the lines behind **Xylem** snap and a piece of wood starts swinging toward
him. He yells "Look out!" and points behind him. **Xylem** does a duck and roll,
_Acrobatics_ @ 22. He grabs the line and ties it down, then checks with the
Captain that it's cool. **Mitor** approves.

**Gadi** is trying to be as useful as possible without succumbing to the
Dunning-Kruger effect. He knows he's not the expert here. **Zasheida** notices
that some of the waves hitting the front of the ship seem as if they are animate
and trying to grasp the ship. _Arcana_ @ 14 reveals a watery creature. We hear
a curse in Orcish from the Captain. The sailors start to arm themselves.

**Gadi** suspects that it's a Water Elemental; **Zasheida** is certain. **Gadi**
believes that it can be communicated with. **Gadi** (knowing that he's not the
most charismatic dude), tells **Radcliffe**, who yells out bird calls. It rears,
and looks down in a confused fashion.

- **Gadi** (@17) casts _Melaku's Detect Enemies_. It works. He yells "It's only
  defending its territory. It's not actively trying to harm us otherwise."
- **Radcliffe** (@11) uses _Entangle_ to make seaweed charades indicating
  peaceful intent. It failed its Int save, but got close. It doesn't understand,
  but likes seaweed. The storminess of the water subsides a bit.
- **Xylem** (@8) goes up to the Captain to make sure we get out of its
  territory. _Insight_ @ 18 helps identify where it came from, so we go the
  other way. **Xylem** thinks the territory is just the squall, moving with it.
  The Captain starts lecturing **Xylem** about how long he's been sailing, etc.
  But he doesn't have a better idea, so complies.
- **Zasheida** (@6) grabs a dagger and holds on to the boat.
- The **Water Elemental** seems to be imitating whatever **Radcliffe** is doing.
  It causes seawater to rise up and crash down. We are all engulfed in water.
  The water is holding us in place as the deck floods. **Radcliffe** loses
  _Concentration_ on his charades.

- **Gadi** regrets not having taken the _Primordial_ language. He also sends a
  _Message_ to the Elemental: "Need air."
- **Radcliffe** summons an incorporeal Hawk Spirit to demonstrate the idea of
  flying to escape the water.
- **Xylem** climbs up the rope, but is having some trouble. Slow progress.
- **Zasheida** climbs the crow's best pole, using her dagger as a piton. She
  gets as far as the normal (non-grappling) water, and can stand to get above
  the waterline. She can see that the water is shaped into a ball, with the
  Elemental in the center.
- The **Water Elemental** pulls the water upward, throwing several PCs into the
  air. Both **Gadi** and **Radcliffe** are able to grab something. **Gadi** is
  hanging with both hands as a wet fire plug, on the same higher spar as
  **Radcliffe**. Fist bump: "Nailed it." **Xylem** swings back next to the
  Captain, who is still swearing and trying to steer.

- **Gadi** sends **Gwadenya** and yells: "There's someone drowning near my
  familiar!" (referring to crew members still trapped in the water ball).
- **Radcliffe** also sees them and wants to prevent them from being washed
  overboard. He casts _Entangle_ to successfully grab one of them.
- **Xylem** swings out to grab the other and get somehow stable. _Acrobatics_
  @ 22 works.
- **Zasheida** helps **Gadi** and **Radcliffe** climb over and down.
- The **Water Elemental** is left standing as the ship moves away from the
  squall. It jumps off into the water and waves goodbye.

**Gadi** finds a crew member to complain to about spilling his beer. The Captain
agrees that we should all have one.

By the time we get to the trading post, it was a Water Dragon that we saved the
crew from. There's a song.

As we arrive, we see that the **Grippli** settlement is in ramshackles, but
there are fairly recent shacks, and we hear some soft croaking, as if the
**Grippli** are conferring. The ship will make repairs as we do what we're here
for. This is obviously a rather hastily-constructed refugee camp, and it's clear
that we are being watched. **Xylem** starts walking toward the encampment in as
inoffensive a manner as he can.

Some sort of **Grippli** Elder comes out with an entourage of guards with spears
that they are not brandishing. She introduces herself as The Pondmother. She
leads a group that has escaped the **Yuan-Ti**. They had to leave many eggs
behind. **Xylem** explains that **Pelk** from **Candlekeep** asked us to come
and provide.

She says that a few weeks ago (when **Candlekeep** noticed), some **Yuan-Ti**
arrived, curious about an old temple of theirs. Things were initially peaceful,
but more arrived, who were more aggressive and evil, punishing both the
**Grippli** and the first group of **Yuan-Ti**. **Xylem** wants to know if they
look different. They only differ in their clothing and whether they are armed.
The highest priority is recovering the eggs. They would like to remain in their
original village, and are happy to co-exist with the initial group of
**Yuan-Ti**. **Gadi**'s questioning suggests that the 2nd group may be affiliated
with the **Cult of the Dragon**, but details are sketchy. **Zasheida** asks if
they were wearing purple. The leader is both purple-colored and is wearing
purple.

To get there, we need to go through the crab maze. The crabs haven't been fed,
so the waters will be quite dangerous. The **Pondmother** gives us basic
directions, locations of her home, the brood pool, etc. The 2nd group of
**Yuan-Ti** fight to the death. The crabs eat crops that the **Grippli**
grow, and suspects that there isn't enough to feed them enough to pacify them.

We might be able to take enough crab food to distract some of them. The water
below the walkways is only slightly above **Gadi**'s head. We could also take a
rowboat.

**Gadi** wants to make cleats for everyone's boots to make it harder to fall off
the slippery planks. **Xylem** also wants to tie us together. **Yuan-Ti** have
good night vision, and magic resistance (vs. saves).

**Gadi** also wants **Radcliffe** to negotiate with the crabs, perhaps unleashing
them to attack the **Yuan-Ti** alongside us. The **Pondmother** is hesitant about
putting the crabs in danger, but is convinced that since they've already lost the
village, it may be worth it. She thinks there were at least 20+ **Grippli**s
still there. **Gwadenya** could deliver a note to one of them.

As we're talking, it occurs to us that we can probably concentrate the attack on
the temple.

**Gwadenya** was able to deliver the message to a smaller group of **Grippli**.
Some of the eggs have been eaten. The evil **Yuan-ti** are concentrated at the
temple, and seem over-confident.

The next morning, we row over to an inlet. There's natural mist and fog, but no
storm. We step onto a gangway, and the crabs obviously notice us. **Radcliffe**
hears them complaining about the loss of their caretakers and their food. Then
questions of "food?" in reference to us. **Radcliffe**: "Hey, you guys understand
me, right?" "Yes. Food?" "Not yet. The food source has been cleared away by
predators. We are trying to scare those predators away so that you can get food
again." "Food." "Soon.", etc. Once he says "Wait", they and the waters calm down.
But he still gets the word back "Hungry."

**Gadi**, **Radcliffe**, and **Zasheida** are wearing the cleats. We go slowly,
talking to the crabs as we go to keep them calm. Then get to the eggs, rescue
them, and then we and whichever **Grippli** fighters want to join us go to the
temple and throw down.

### Getting across the crab maze

Marching order: **Radcliffe** first. _Perception_ @ 17 reveals that some of the
planks appear to be unnaturally rotted, as if intentionally weakened. He tells
us. It looks to be from some sort of spell. We can examine them to get advantage
on finding new ones. The crabs are not communicating amongst themselves, so he
needs to keep reassuring new ones.

We eventually get to a point where we would expect some crabs, but they don't
appear to be there. **Radcliffe** asks if anyone is down there. He does not get
an answer. **Gadi** casts _Melaku's Detect Enemies_, and gets an overwhelmingly
strong response. He tells **Zasheida** in **Dwarvish**, **Xylem** in **Elvish**,
and then **Xylem** can relay to **Radcliffe** in **Druidic**.

The crabs on the immediate other side are getting agitated. They seem to have
been _Charmed_ or _Suggested_, or some other magical thing. **Radcliffe** sees
a trap and skips over it, and tells **Gadi** to "Jump!". He complies, and we
see claws snapping beneath, where he was. He crashes into **Radcliffe**, and we
go tumbling. **Xylem** jumps to another pathway. **Zasheida** jumps to the same.

We make our way down to a close point where we can wade about 20ft to get to a
slightly further south point on the peninsula. **Radcliffe** summons a
cuttlefish to scope out the waters, seeing a **Yuan-Ti** swimming alongside the
planks. It's approaching, but isn't very near us. **Gadi** uses _Mold Earth_ to
make a land bridge and try to block the swimmer. **Zasheida** realizes that we
need to deal with this **Yuan-Ti** to prevent an alarm.

## For next session

Kill the swimmer, who **Zasheida** thinks is the only scout.

