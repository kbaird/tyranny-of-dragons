# 2022-06-26 Session, Session 3

## From previous sessions

- [2022-04-17](2022-04-17.md)
  - We all gathered in **Greenest** while the **Cult of the Dragon** attacked
- [2022-05-21](2022-05-21.md)
  - **Escobert** dueled, and we took off after a wounded **Langdedrosa**
- [2022-05-28](2022-05-28.md)
  - Follow **Langdedrosa** & go to the camp, rescuing the prisoners and
    liberating the Dragon eggs.

## Events of this session

Go with **Leosin** to **Candlekeep**.

**Radcliffe** goes shopping. **Gadi** does calisthenics and invites the others.
**Radcliffe** ends up tripping over his own feet and not learning anything.
**Xylem** does higher-level Monk stuff. (**Gadi** got a _Performance_ @ 17).
People gather and think that it's more like _capoeira_. **Gadi** gets annoyed.
**Dasaadi** has a Dragonchess board set up, and watches with amusement.

A few days pass. More of the same.

**Gadi** confers with **Escobert** and **Leosin** if there's anything they'd
like him to be doing. **Escobert** gives **Gadi** a letter to take to someone
in **Baldur's Gate** (details TBD in some later session: perhaps it's a letter
of introduction to a **Harper**, thinking that he's a good candidate. He does
not tell **Gadi** this).

Similar details TBD for the **Emerald Enclave** for **Radcliffe** or **Xylem**.

**Gadi** also inspects weapons and whetstones, etc. **Dasaadi** is initially
amused, but comes around to enthusiasm, and slaps **Gadi** on the back.

### On the road...

**Leosin** is in the lead, and **Gadi** is up there to chat with him about
**Candlekeep**. Chris says that there's a minor road that cuts just south of
**The Wood of Sharp Teeth** that's more direct. On that route, we start to hear
sounds of activity. **Zasheida** goes scouting on foot with **Gwadenya**.

_Survival_ @ 24 allows her to blend in with the tall grass easily while also
moving very quickly as she approaches the top of a nearby hill for observation.
**Leosin** goes scouting separately. **Radcliffe** conjures his own hummingbird
familiar, which also runs off separately.

**Zasheida** sees a caravan that's had a wreck. There are a couple heavy wooden
wagons on the road, and one in a ditch. The people are small: Gnomes? There's
steam coming from the wagon in the ditch. There's no sign of foul play.

**Radcliffe**'s hummingbird (**Aegisess / "Eggie"**), flies right up and flits
around. He thinks that the Gnomes are just Human children. Where are the parents?
He rushes in. **Zasheida** more calmly accompanies.

The Gnomes are very happy to see **Radcliffe**, and gesture toward the wagon
while continuing to speak in Gnomish. He fails to force open the door. **Z**
uses her scimitar to break the lock. There's a rush of air going into the wagon
and a green fog rushes out, enveloping them and several Gnomes.

**Radcliffe** gasps and inhales, and immediately feels dizzy and silly, but
keeps control of himself. It's sort of like Joker gas. Phil explains his success
as having been trained to avoid toxins. **Zasheida** is more affected, having
Disadvantage on checks. Everything is funny to her.

**Gadi** gets a 1 on _Perception_, and assumes the worst: deadly poison.
**Dasaadi** mostly agrees. She rushes in. He uses _Mold Earth_ to create WW1
trenches to avoid the lighter-than-air gas.

**Xylem** yells "It's OK! We're all OK in here. It's just laughing gas!"
**Gadi** calms down in response.

**Zasheida** checks out the inside of the wagon: _Investigation_ @ 2. There's
an unconscious Gnome on the floor, surrounded by alchemical supplies. Mo says
that she starts laughing hard enough to hurt her stomach, and falls over, getting
Inspiration.

**Radcliffe** yells "Are you OK in there?" His hair is now fluorescent green
due to the gas. Various Gnomes come out, falling into the trench, laughing.

**Gadi** tries to make conversation with a Gnome, but he's too stoned to be
useful. There appears to be one lucid Gnome, who casts a gust of wind to clear
the air. **Gadi** approaches her and tries to converse again. They're brewers,
and he brings up his brother **Kaleb**. They actually have a few cases of
_Riftic Stout_. We continue chatting.

**Zasheida** pulls the unconscious chef out of the kitchen wagon. **Radcliffe**
casts _Cure Wounds_ for 5HP as he regains consciousness. As he opens his eyes,
he addresses **Gadi** by **Kaleb**'s name. Apparently they worked together,
and the Gnome is working on his own variant of _Riftic Stout_.

**Radcliffe** is still confused about the lack of parents, and asks **Gadi**
what's happening. As **Gadi** explains and uses the word "Gnome", **Radcliffe**
looks it up in his notebook.

**Mr. Bunnyman** shows up in the tall grass, and **Eggie** darts over to
"attack". He finds it very funny. **Radcliffe** goes over to chat. **Mr. B** is
really amused that "Candlekeep" doesn't keep candles at all, but books. They
have a big laugh, and suggest that it should just be called "The Library".
**Radcliffe** gives **Mr. B** a carved **Mr. B** totem, which he puts away in a
leather pouch.

We make camp here for the night with the Gnomes. The master brewer gives him a
book that they would like **Gadi** to give to his brother. They continue toward
**Candlekeep** when we split to the left toward the coast. We also encounter
various other travelers in both direction.

We're about a day out when a group of about 5 approaches on galloping horses.
They're a stereotypical adventuring party. Their Paladin leader offers help.
**Gadi** gets off his pony to have a forthright LG conversation. The rest of
both parties are annoyed and impatient. **Gadi** informs "Dudley Do-Right"
that we are the **Team Characterized by Dependably Stalwart Competence**. He's
suitably impressed.

### Candlekeep

We eventually make it to **Candlekeep**. Chris describes. It's the largest
repository of written lore in all of Faerûn, and is run by the **Avowed**.
Visitors are called **Seekers**. The Priests of **Deneir** (the God of Writing)
oversee the gates at the front. They're 18ft tall black metal emblazoned with
runes of protection.

**Leosin** tells us that the only way in is through the front gate. It has
various magical protections for the contents, etc. We'll need to sign various
waivers.

At the gates, 5 dramatically purple-robed figures come out. One steps forward
to grant access to **Leosin**. He waves for us to follow, but they object. They
argue, but ultimately **Leosin** loses. We can only enter if we give them a
book that they don't already have.

**Gadi** and **Radcliffe** chat about this and their books. The main robed
figure approaches and assures **Radcliffe** that he can put rules on who is
allowed to read anything he wrote in Druidic (**Radcliffe**'s teacher was
very clear about limiting access). **Radcliffe** offers his personal logbook,
which looks like it was made by a 5-year-old. They are quite honestly
reverent toward it. Only other Druids can read from it or copy its content.
They accept it, and give us all entry wristbands as Seekers. One pulls
**Radcliffe** to the side and gives him a nice big blank book with various
protective runes for weather and travel. "What a thoughtful gift!" He's pretty
jazzed.

We all enter the **Court of Air**. There are various buildings catering to the
needs of travelers, and the **Emerald Doors** that we can't enter.

We stable the horses, check in, get some food, etc. There are other scholarly
travelers here, too. The main door into the tavern common room opens, and a
robed Dragonborn scholar approaches us. She introduces herself as **Pelk**,
and asks about our involvement with **Greenest**. She joins us, and explains
that **Candlekeep** relies on a community of **Grippli** (frogfolk), with whom
they trade seafood and other things. They've gone away, apparently. They would
like us to investigate, offering 500GP per person and permanent Seeker status.

**Leosin** and **Dasaadi** offer to stay behind and continue research into
**Project Make Things Cool Again** (**Radcliffe**'s codename for stopping the
**Cult of the Dragon**).

#### The Book of Cylinders

**Pelk** shows us the **Book of Cylinders**. When opened, it literally contains
3 wooden cylinders with various carved inscriptions written in **Dwarvish**
(image in #media). It apparently pertains to the **Grippli**.

**Radcliffe** immediately examines them very intently. It's clear to him that
the characters are backwards. If rolled to make an imprint in clay (enough of
which is contained in the book), it would make a forward-facing text in
**Dethek**. **Zasheida** also figures out the order of cylinders. **Gadi**
suggests waiting until we're in a private room, instead of doing it in the
common room of the tavern.

The room has a magical silence, preventing sounds from carrying into the
hallway.

Cylinder1 tells of how the **Grippli** learned to harvest giant crabs, and
gained access to wealth.

Cylinder2 tells of serpent creatures hovering ominously near the **Grippli**
village, which falls into disarray. **Gadi** suspects that they are Yuan-Ti.

Cylinder3 depicts the aftermath of the attack: the village is vacant, and the
temple has been taken over by the invaders.

The **Grippli** live on the coast near the **Cloakwood**, a bit north of
**Candlekeep**. They offer us a ride via ship.

## For next session

Go help the **Grippli**.
