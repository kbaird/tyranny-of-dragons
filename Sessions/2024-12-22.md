# 2024-12-22 Session, Session 34, ?

[Previous Sessions from _Hoard of the Dragon Queen_](Previous-HoD-Sessions.md)

- [1490-01-08](2023-10-29.md):
  - The **Team Characterized by Dependably Stalwart Competence** saves
  **Skyreach** and returns to **Waterdeep** for debriefing. While there, the
  **Drakhorn** calls all Chromatic Dragons to war. We are given a guide and
  horses, and asked to meet a contact at **Boareskyr Bridge** to deal with
  **Neronvain**, who has been attacking Elves in the forest with some Draconic
  allies.
- [1490-01-20](2023-11-26.md):
  - The **Team Characterized by Dependably Stalwart Competence** makes their way
  to **Boareskyr Bridge**, guided by local Yuan-Ti to the **Tomb of Diderius**,
  which **Varram the White** has already entered with this barbarian mercenaries.
- [1490-01-20](2023-12-17.md):
  - The **Team Characterized by Dependably Stalwart Competence** encounters
  **Merrshaulk**-worshipping Yuan-Ti inside the **Tomb of Diderius**, tracking
  **Varram the White**.
- [1490-01-20](2024-01-21.md):
  - The **Team Characterized by Dependably Stalwart Competence** brings
  **Varram the White** back to **Waterdeep**.
- [1490-01-20](2024-02-03.md):
  - The **Team Characterized by Dependably Stalwart Competence** learns more
  myth arc dangers and is forced into the **Yawning Portal** in the eponymous
  tavern.
- [1490-01-20?](2024-03-03.md):
  - The **Team Characterized by Dependably Stalwart Competence** meets **Amos**
  the Duergar in the **Dungeon of the Mad Mage**. **Amos** transports us to the
  **Shadowfell**.
- [no one else cares](2024-04-14.md):
  - The **Team Characterized by Dependably Stalwart Competence** learns that
  **Radcliffe** has been in the **Shadowfell** for ~1 year, and starts working
  for **Bahamut** and his steward **Umbraxakar**, making our way to the temple.
- [no one else cares](2024-04-28.md):
  - The **Team Characterized by Dependably Stalwart Competence** reunites with
  **Radcliffe** (who had already dealt with the **Ice & Despair** pillar) and
  deals with the **Nightmare & Fear** pillar.
- [no one else cares](2024-06-23.md):
  - The **Team Characterized by Dependably Stalwart Competence** deals with the
  **Decay & Entropy** pillar, healing **Nyssa** and acquiring a dark red gem.
- [no one else cares](2024-07-07.md):
  - The **Team Characterized by Dependably Stalwart Competence** encounters
  **Strahd** in the **Obsession & Madness** pillar, where **Tiamat** was
  masquerading as **Tatiana**. We help him pierce the illusion, allowing him
  to destroy the **Tatiana** golem. He then acquiesces to being _Banished_
  back to **Barovia**, then discovering that he's entirely alone there.
- [No Paul](2024-07-28.md):
  - **Radcliffe** established **The Ruby Enclave** at **Red Haven** and got married.
  - **Gadi** got married, had a daughter, and leads a regiment of **Spellborn War Mages**.
  - **Zasheida** is in more direct command of ^, and leads aerial raids under **General Mulu**.
  - **Chadrick** was trained by **Gadi**'s mom.
  - **Blagothkus** guards **The Trade Way** south of **Waterdeep**.
  - **Leosin** has been doing spy stuff.
- [Paul's Back](2024-08-18.md):
  - The PCs flew to the source of the **River Reaching**, restoring its flow to **Boulder's Gate**.
  - We also discovered a underground cathedral/forge, where **Demogorgon** orders **Duergar** to
    build an artificial body for possession by **Tiamat**. **Zasheida** thinks they're an army.
    We take detailed spy notes and return to **Red Haven**.
- [Atlantis!](2024-09-08.md):
  - The PCs destroy The Cult's undersea base and make contact with **Bethchadjuharazeb** the
    demon, who wishes to ally with us against the Cult.
- [Free Waterdeep!](2024-09-22.md):
  - The PCs free the city of **Waterdeep** from the command of the Mind Flayer **Ligdan**, who
    agrees to let us take the risk of fighting **Tiamat**.
    - **Gadi** & **Xylem** stay in the city to help refine the device.
    - **Radcliffe** feels a little guilty and wants to be similarly helpful.
    - **Zasheida** stays to help with the "de-Nazification" process.
    - **Leosin** hands **Gadi** a **Calishite** scroll case and says "We could have a problem."
- [Calimport](2024-10-06.md): **Zasheida** has inspired a reform movement in **Calimshan**, and
  has a dream where **Nurril** speaks to her in **Draconic**. It's very _Dune_. **Nurril** morphs
  into the **Obsidian Watcher** Wyvern, who says "Come find me." (in **Memnon**), where there are
  artifacts that may be helpful in the fight against **Tiamat**: either the **Rod of Memnon** or
  the **Tears of Calim**. **Zasheida** has also had a Magical Girl experience connecting her to
  all of the **Zasheidas** that have ever been, including **Hailia**. She know has a plethora of
  knowledge about how to fight **Tiamat**, especially physically.
- [Memnon](2024-11-10.md): We meet with the Wyvern, are given various artifacts, and then go to
  **Red Haven** to deal with **Severin**.

## Events of this session

Spies have come back and described the 5 temples that **Tiamat** will use to re-enter the
Material Plane. They're arranged in a circle, and armies are gathering. At one of them, there
were hundreds of tattooed cult members gathering and chanting. The spy wrote down some of the
glyphs. **Gadi** recognizes them as **Conjuration** runes. The tattooing is unusual. Our other
spies confirm that this is happening at all of the temples. (The 5 temples are also specific to
a given head/element of **Tiamat**).

The PCs are gathered in **Red Haven**. Phil thinks it's still somewhat short-term focused. It
probably won't continue after the big battle.

**Xylem** knows a demon who sends him a message that various demons are missing, and they suspect
it may relate to **Tiamat**'s plan. He wants to find out if other outer planes denizens are also
missing. **Radcliffe** checks on the Fey. None are missing, but they have also heard about missing
Devils & Demons, and it suggests even more strongly a connection to **Tiamat**, because the
absences are focused on **Tiamat**'s plane (**Avernus**). This has even lead to the Angels gaining
the upper hand in the **Blood War**, due to the reduction in count of both Devils and Demons.

**Zasheida** has also been doing some surveillance. Over the desert, it was easy at first (little
interference). But it's getting more crowded in the sky now with various magical creatures. (She
may have been the original source of info about the temples having specific groups associated with
them). The arcane defenses are becoming much more sophisticated.

We decide to go to the cold-themed temple. Everyone's _Perception_ rolls but **Xylem**'s are
terrible. He notices that it's built to look like it has a single entrance, but he notices some
secret ones, as well. They appear to be mechanical. He also notices activity.

**Zasheida** realizes that the arrangement of the entrances suggests a specific function (use by
a given type of creature). We're considering the entrance mostly used by **Frost Salamanders**,
not known for great senses. They have Tremorsense, so if we float past invisibly on a Flying
Carpet, we can avoid that.

We make our way past a Frost Salamander on the way in, eventually making our way to the center.
A dragon could easily fit in this space. There are hundreds of cultists gathered, sitting, facing
in. We take stock of our surroundings:
- **Xylem** notices that the cultists have specific places/patterns, and are not fully lucid.
  Their arrival at specific places is verified by someone very much like an orchestra conductor
  who is consulting some plans. There is no obvious communication to the crowd.
- **Gadi** remembers older rituals channeling arcane energies for sacrifice, and also notices
  similarities to stonework at other cult locations. This stonework is reinforced on the inside
  against extreme pressure.
- **Zasheida** sees that there's a cult member in Purple w/a Red Wizard, and they're reading
  from some unholy text.

**Gadi** gets a 30/"Fantastic" (nat 20) on **Arcana**, feeling the magical energy surge up from
below. **Xylem** was watching the cult members, and sees all of the tattoos on them start to
float out of their bodies and combine together into a sort of spiderweb shape. He then hears
euphoric demon cries. **Gadi** is aware that this is still going to take some time.

**Zasheida** notices that there are guards watching for interference as soon as the arcane
energy surges, and passes this info on to **Xylem** before his _Misty Step_ shenanigans.
**Radcliffe**'s old Patron whispers to him that "it's happening now" and he needs to be careful.

**Xylem** is using _Misty Step_ and _Mage Hand_ to get to where he can do non-attack damage to
the unholy book. He's greeted telepathically by **Tiamat**, but he makes his Wisdom save.

**Gadi** and **Radcliffe** are sabotaging the funnel shape, and a Frost Spider starts climbing
down on a thread. **Radcliffe** teleports up and back, causing a _Thunder Step_ that turns the
spider into spider guts that splat all over **Gadi** while he finished the sabotage.

**Zasheida** can shoot at the scaffolding holding up the guards, **Arondir**-style. It makes one
fall off, and she then shoots at the one who didn't fall.

**Xylem** uses _Mage Hand_ to grab the book, and he's playing tug-of-war with the Purple Cultist
and the Red Wizard. **Radcliffe** then casts _Maelstrom_ on the area of the tug-of-war, causing
6d6 damage. **Radcliffe** got a nat 20 on his Wisdom save vs. **Tiamat**'s voice in his head,
which allowed him to follow through on the spell, which has a large enough area to also affect
the front rows of regular cultists. They also let go of the book.

**Gadi** swoops down so that **Xylem** and **Radcliffe** can teleport back on. The floor erupts
into a pillar of magical frost. It rips a whole into Hell. We all take damage (**Gadi** took 21)
from the ritual partially completing. A malformed avatar of **Tiamat** (like the transporter
accident in _Star Trek I_) appears. As the malformed partial avatar flails around, destroying
the temple, we fly away.

Back at **Red Haven**, we figure things out after the fact. The final avatar of of **Tiamat**
will lack the white head because of what we did personally, and we direct SG-1, SG-2, etc. to
do the same for the other heads.

## For next session

Go to Hell. Bring **Chadrick Bradrick**. Go up a level or 2 beforehand.
