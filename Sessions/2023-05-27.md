# 2023-05-27 Session, Session 14

> Just because our enemies are well-dressed, and we oppose them, that's no
> excuse for a slovenly appearance.
- **Gadi Melaku**

## From previous sessions

- [2022-04-17](2022-04-17.md)
  - We all gathered in **Greenest** while the **Cult of the Dragon** attacked
- [2022-05-21](2022-05-21.md)
  - **Escobert** dueled, and we took off after a wounded **Langdedrosa**
- [2022-05-28](2022-05-28.md)
  - Follow **Langdedrosa** & go to the camp, rescuing the prisoners and stealing
  the Dragon eggs.
- [2022-06-26](2022-06-26.md)
  - Met Gnomes on the way to **Candlekeep**, accepted the job to look into the
  **Grippli** village.
- [2022-08-28](2022-08-28.md)
  - Went to the **Grippli** village, started the stealthy approach.
- [2022-09-11](2022-09-11.md)
  - Rescued the eggs and met up with the **Emerald Enclave**.
- [2022-10-02](2022-10-02.md)
  - Returned to **Candlekeep**, went to **Baldur's Gate**, fought the
  **Rotting Tongue Gang** in the sewer. Set up a meet with their shady
  associates, who we think are affiliated with the Cult.
- [2022-10-23](2022-10-23.md)
  - Met with **Acklyn Salaban** to sign on to the caravan under assumed names.
  - Met with **Onthar Frume**, **Gadi** joined the **Harpers**.
  - Got transported to **Barovia**.
- [2022-11-27](2022-11-27.md)
  - Inscribed the **Amber Temple** and brought **Nebhotep** to **Barovia**
  - Collected **Argynvost**'s soul
  - Killed 2 cultists and acquired the broken half-mask
  - **Gadi** passed **Col. Mulu**'s test by returning the hammer
- [2022-12-18](2022-12-18.md)
  - We brought the **Silver Dragon Mask** to **Candlekeep** and did various
  research. We joined the caravan, met **Jamna Gleamsilver**, and learned that
  the 3 Cult wagons are transporting stolen booty to be used to bribe various
  chromatic Dragons to ally with them.
- [2023-01-08](2023-01-08.md)
  - We entered "The Fields of the Dead", rescued **Carlin Emoffel** (the
  **Harper** who had been buried up to his head), and encountered the Myconids
  under the road. We made our way to a few days' travel south of **Waterdeep**.
- [2023-02-12](2023-02-12.md)
  - **Carlin Emoffel** was revealed to be a double-agent. We helped **Jamna**
  rob the best items from the caravan loot, looked like Big Damn Heroes, and
  made our way into **Waterdeep**.
- [1489-12-04](2023-03-05.md):
  - We make our way north along the High Road to a compound called the
  **Carnath Roadhouse** near the **Mere of Dead Men**. We befriend a Lizardfolk
  named **Snapjaw**, who tells us about the activities of the
  **"Dragonkneelers"**.
- [1489-12-1x](2023-04-30.md):
  - We investigate the barn and meet **Voaraghamanthar**, agreeing to work against
  the Cult. We are making our way to the hunting lodge and **Skyreach Castle**.

### TODO / Setup

## Swag

- Disguise kits for the caravan journey
- **Zasheida** has a book from **Candlekeep** about the Cult
- Various spores from the Myconids
- Chris is treating Phil's stolen backpack as a Cypher
- https://www.dndbeyond.com/homebrew/creations/view?entityTypeId=112130694&id=6034755

## Events of this session

Probably start with a camp out a safe distance from the hunting lodge, using **Sending**
to coordinate with **Jelen** before moving in. We have been directed to observe for a
while. It is now the first of the year 1490.

**Radcliffe** took a level of Druid, which he explains as having scouted the area in the
form of a mountain goat. He's focusing on learning the lay of the land, rather than
scouting the lodge specifically. He has not run into anyone hunting or foraging, which
seems suspicious.

**Zasheida** does standard patrols around our camp, and observes that the wildlife does
not appear to be afraid of the lodge (which corroborates that there is no hunting going
on). She also does not notice any humanoid footprints, but has seen a few Trolls that
operate out of the lodge. If there is a guard schedule, it's light, at least with regard
to things outside the lodge. At night, she has seen a few lights come on, and perhaps a
patrol or two with torches. They don't seem to have a consistent schedule. No one has
approached from outside on foot or horse, either. There are several old lichen-covered
stones in the lodge's front yard, and seem out of place. **Gadi** gets 22 on _Arcana_,
concluding that they may be some sort of gate, very old, and possibly the work of
Giants.

**Xylem** is practicing his Astral connections, specifically figuring out how to dash
and disengage and hide (reflecting taking a 2nd level in Rogue). He also does some
scouting at night. He asks **Zasheida** to help him with sparring, specifically his
disengage. On his night scoutings, he notices somewhere in the basement of the lodge
an oddly-colored light source.

**Gadi** wants to nickname all the guards we see (3 Stooges), and gained the
**Manifest Mind** feature. He wants to cast _Invisibility_ on one or the other of them
to do recon, with decreasing distance, and eventually being **Xylem** himself. He can
see that the Giant portal is probably damaged, and has partly fallen over (it's
basically a broken Stargate). We see that there are Ambush Drakes in the kennel.
At the windowsill, **Gwadenya** sees a fair number of priests in scarlet red robes
and tattoos, clearly identifiable as Thayan Red Wizards.

**Gadi** gives **Radcliffe** the "first paragraph of the Wikipedia article" infodump
on them. He then consults with his patron for more detail. In response, a massive Fey
moose approaches our camp, clearly very specifically focused on **Radcliffe**. They
chat in Sylvan. We are largely nonchalatant. **Gadi** gives it some beer in a dish,
which it loves enough to sit down next to its new friend. He continues to refill the
dish while it chats with **Radcliffe**. **Radcliffe**'s Patron **Cordia Sebestina**
tells him that she has sent an emissary bearing gifts, which **Radcliffe** is worried
about, thinking that we would become bound to her if we accept them. She says that any
obligation would only be incurred by **Radcliffe** alone. The Fey Moose has 4 wreaths,
each clearly meant for one of us: having specific flowers from the Rift, or **Xylem**'s
Druidic order, etc. **Radcliffe** immediately claims all the wreaths personally, and
only then distributes them to each PC.

The wreaths have Protection spells on them, providing Advantage on saves. **Xylem**
picks Cold, **Zasheida** picks Fire, **Radcliffe** picks Lightning, and **Gadi** picks
Necrotic. We all thank the moose, and **Cordia** tells **Radcliffe** that we are not
ready to face the Red Wizards, even with this protection. If we see one, run.

**Gwadenya** had seen 2 Red Wizards speaking to our old nemesis **Frulam Mondath**,
and that Frulam seemed to be taking orders from them, suggesting that they are more
powerful. There was also a new female Elf dressed in armor that looks well suited
for arctic environs (possibly made from White Dragon scales).

**Xylem** goes on a scouting mission while _Invisible_, entering via a balcony on the
2nd floor (of 3 + basement). The Elf in white armor (named **Talis**) is complaining to
**Frulam** that she was promised to go up in status if she provided access to the gate.
**Frulam** replies that she needs to bring it up with the Red Wizards, who are in charge.
This room and its balcony are clearly **Talis**' living quarters. **Xylem** is going to
avoid trying to break into any private rooms, and mainly get a lay of the land. He goes
down to the 1st/ground floor.

There's some really gross chamber which the Trolls have claimed as their room. There are
various other guards, but it still seems pretty lightly guarded, as if there's only some
pretense of security. He then continues down into the basement. Typical cold storage.
However, he immediately sees a large dropoff into a natural cavern, with lots of trappings
of industrial operations. In this large cavern, there are 3 portals. The Thayan Red Wizards
walk through the center one; the left received some cargo and looks cloudy; and the right
one seems to open onto a rural road of some sort. **Xylem** got an 18 on _Stealth_, but
even so, one of the Red Wizards seems to make eye contact and smile at him. He backs away.
The carts going into the left portal simply stay on tracks: there is no "unloading" per se.

He sees a purple-scale armored male Human walk in to bark some orders at the Trolls on the
first floor. He finds a room with a window whose door lock back into the main hallway he can
disable. He then leaves out the window, leaving that accessible as well.

**Gadi** receives a **Sending** on behalf of **Colonel Mulu**. She and **Desaadi** have
traveled to **Waterdeep**, where they are representing the TCbDSC at council meetings.
We are directed to continue forward to learn what lies through one of the portals. Chris
also wants PC-specific news to be reported back, such that it sounds coincidental, but if
it's not, it's super-creepily specific to us.
- **Radcliffe** hears about the source of his human appearance being seen in the Great Rift.
- **Gadi** hears about someone arriving in the Rift named **Radcliffe**, who looks just
  like ours, except brunette.
- **Xylem** gets a report that his connection to his bracelet may eventually lead to a
  separation from his monastery.
- **Zasheida** hears that the location of **Hailia**'s discoveries was in **Barovia**
  (which we are also familiar with from the Silver mask trip).

**Gadi** mentions to **Radcliffe** that he heard of a doppelganger appearing in the Great
Rift. **Radcliffe** seems quite agitated, and asks many questions. He wants to make contact
with this person, whom he has never met. **Gadi** offers to send directly to the "real"
Radcliffe on his behalf.

The guilds of **Waterdeep** offer to send us an airdrop via griffon. **Gadi** asks for the
decision-makers to notarize why they consider **Gadi** worthy of such treatment, so that he
can use that to argue his case with the **Tesfayes**.

**Gadi** is going to cast _Invisibility_ on **Xylem**, joined by **Zasheida** with a Cloak
of Elvenkind. They sneak downstairs, and see **Talis** going through the right portal,
seeming to be in some sort of a hurry. They follow, finding themselves on a small platform,
overlooking a small village. There is also a large ice castle, built to Giant scale. They
also see 2 statues before an enormous gate. They see **Talis** riding a horse that was
waiting for her toward the castle. They are right next to the town, and go in to check it
out. They can tell from signs that the village is named Parnast. They overhear people talk
about the Cult of the Dragon, being complimentary in the way that people living under evil
overlords feel forced to. The population is typical mixed humanoid, with no Giants and no
Dragons. Someone says "Talis is back, and will want to go on a hunt soon. We should get the
guides together." The Cult has controlled Parnast for more than a year. They keep Wyverns.
There is a **Captain Ofalstan**, who is apparently the guy who gave orders to the Trolls.
There is some mention of Dragonborn.

**Zasheida** is able to tell from mountain and desert landscape features that **Parnast**
is in the **Greypeak Mountains**, up the **Delimbiyr River** east of **Daggerford**. We
report this back via _Sending_.

We also go south a bit to use _Mold Earth_ to enter the tunnel partway and get ourselves
into one of the carts. The Lizardfolk tell us that the carts enter a shelter that can not
really be seen out of, and that they are responsible for dumping cargo on the other side.
Chris says we just get through.

Paul wanted **Xylem** to scout out the lodge for evidence collection purposes before we
finally go through the tunnel. Chris says that's fine. **Xylem** finds a discarded letter
that **Talis** wrote and then decided against sending, etc.

We go through the left portal. We're in a small room, looking out onto an upper courtyard
on a big floating rock with a castle.

We see a pair of Stone Giants (the unloading crew) walking towards our room.

## For next session


