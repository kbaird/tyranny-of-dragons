# Previous Sessions from _Hoard of the Dragon Queen_

- [2022-04-17](2022-04-17.md)
  - We all gathered in **Greenest** while the **Cult of the Dragon** attacked
- [2022-05-21](2022-05-21.md)
  - **Escobert** dueled, and we took off after a wounded **Langdedrosa**
- [2022-05-28](2022-05-28.md)
  - Follow **Langdedrosa** & go to the camp, rescuing the prisoners and stealing
  the Dragon eggs.
- [2022-06-26](2022-06-26.md)
  - Met Gnomes on the way to **Candlekeep**, accepted the job to look into the
  **Grippli** village.
- [2022-08-28](2022-08-28.md)
  - Went to the **Grippli** village, started the stealthy approach.
- [2022-09-11](2022-09-11.md)
  - Rescued the eggs and met up with the **Emerald Enclave**.
- [2022-10-02](2022-10-02.md)
  - Returned to **Candlekeep**, went to **Baldur's Gate**, fought the
  **Rotting Tongue Gang** in the sewer. Set up a meet with their shady
  associates, who we think are affiliated with the Cult.
- [2022-10-23](2022-10-23.md)
  - Met with **Acklyn Salaban** to sign on to the caravan under assumed names.
  - Met with **Onthar Frume**, **Gadi** joined the **Harpers**.
  - Got transported to **Barovia**.
- [2022-11-27](2022-11-27.md)
  - Inscribed the **Amber Temple** and brought **Nebhotep** to **Barovia**
  - Collected **Argynvost**'s soul
  - Killed 2 cultists and acquired the broken half-mask
  - **Gadi** passed **Col. Mulu**'s test by returning the hammer
- [2022-12-18](2022-12-18.md)
  - We brought the **Silver Dragon Mask** to **Candlekeep** and did various
  research. We joined the caravan, met **Jamna Gleamsilver**, and learned that
  the 3 Cult wagons are transporting stolen booty to be used to bribe various
  chromatic Dragons to ally with them.
- [2023-01-08](2023-01-08.md)
  - We entered "The Fields of the Dead", rescued **Carlin Emoffel** (the
  **Harper** who had been buried up to his head), and encountered the Myconids
  under the road. We made our way to a few days' travel south of **Waterdeep**.
- [2023-02-12](2023-02-12.md)
  - **Carlin Emoffel** was revealed to be a double-agent. We helped **Jamna**
  rob the best items from the caravan loot, looked like Big Damn Heroes, and
  made our way into **Waterdeep**.
- [1489-12-04](2023-03-05.md):
  - We make our way north along the High Road to a compound called the
  **Carnath Roadhouse** near the **Mere of Dead Men**. We befriend a Lizardfolk
  named **Snapjaw**, who tells us about the activities of the
  **"Dragonkneelers"**.
- [1489-12-1x](2023-04-30.md):
  - We investigate the barn and meet **Voaraghamanthar**, agreeing to work against
  the Cult. We are making our way to the hunting lodge and **Skyreach Castle**.
- [1490-01-01](2023-05-27.md):
  - We investigate the lodge, communicate through various _Sendings_, discover that
  the right portal goes to **Parnast**, the middle to **Thay**, and the left to a
  castle in the sky, which we follow.
- [1490-01-02](2023-07-22.md):
  - We arrive in the Ogre Barracks on **Skyreach** and sneak our way over to
  **Sandesyl**'s tower. She will arrange a meeting between us and **Blagothkus**,
  the Storm Giant who resents the presence of the Cult on **Skyreach**.
- [1490-01-03](2023-08-13.md):
  (late evening): We scoped out **Skyreach Castle** and met with **Blagothkus**,
  convincing him (despite **Gadi**'s involvement) to ally with us, mostly due to
  his fondness for **Radcliffe**. Tentative plans to help **Blagothkus** take
  control of **Skyreach** from both the Cult and **Glazhael** (the White Dragon),
  with him then being a future ally against the Cult. Various _Sendings_;
  **Voarag** is on his way, etc. **Blagothkus** plans to call some decoy meeting
  so we can sneak into **Rezmir**'s room to steal the Mask.
- [1490-01-04](2023-09-10.md):
  Next evening, we destroy the Black Dragon Mask and drive away **Glazhael**,
  while **Rezmir** and **Rathmodar** get away, sabotaging **Skyreach** such that
  it is falling. We need to survive and get to **Waterdeep**.
