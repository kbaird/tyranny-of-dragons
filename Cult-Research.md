# Cult Research

We will be acting as guards for the caravan that is run/led by the cult, and
should gather info while doing that.

## We Know
- It is organized into cells, rather than a hierarchy. There may be multiple
  cells in **Baldur's Gate** that are unaware of each other, for example.
- Leaders wear purple.
- It is increasingly conducting raids to fundraise.
- The dragon attacking **Greenest** was being controlled, probably via the mask.
- They had dragon eggs at the camp.

## We also do 2-pronged research at Candlekeep:
- **Gadi** does **Religion** @ 25
- **Xylem** does **History** @ 24

### Together, we learn:

(Mostly piecing things together from research about cults in general and
dragons in general.)

- Organized into cells, varied in size
- Currently led by a Calishite named **Sevrin Silrajin**, who believes that
  Draconic knowledge and power belongs to living dragons, not undead ones.
- There are 5 (chromatic) dragon masks that when assembled release **Tiamat**
  from her prison in the Nine Hells. (Our silver mask was an experiment, not
  needed for the releasing).
- **Szass Tam** (known generally as the leader of **Thay** and the
  **Red Wizards**) may differ with **Sevrin Silrajin** about final results, but
  is likely also helping to free **Tiamat**. This is bad.
- We also find
  [The Elegy for the First World](https://www.dndbeyond.com/sources/ftod/elegy-for-the-first-world#ElegyfortheFirstWorld)
- The cult is many centuries old, but has recently become more powerful under
  **Silrajin**.
